//
//  PostTableViewCell.swift
//  Textmagic
//
//  Created by Stan Cheung on 3/20/18.
//  Copyright © 2018 Stan Cheung. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        print("cell selected!")
    }

}
