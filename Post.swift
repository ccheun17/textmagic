//
//  Post.swift
//  Textmagic
//
//  Created by Stan Cheung on 3/21/18.
//  Copyright © 2018 Stan Cheung. All rights reserved.
//

import UIKit

class Post {
    
    //MARK: Properties
    
    var title: String
//    var photo: UIImage?
//    var rating: Int
    
    init(title: String) {
        self.title = title
    }
    
    
    
}
